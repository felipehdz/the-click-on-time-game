package com.marcfarssac.myapplication.util;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;


public abstract class ResultsTextView extends AppCompatTextView {

    public long winnerClicks = 0;
    public long loserClicks = 0;

    public ResultsTextView(Context context, AttributeSet attrs){
        super(context, attrs);
    }

    abstract public void onClick(long time);

}
