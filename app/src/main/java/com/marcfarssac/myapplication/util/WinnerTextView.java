package com.marcfarssac.myapplication.util;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;

public class WinnerTextView extends ResultsTextView implements ResultsStyling {

    public WinnerTextView(Context context, AttributeSet attrs){
        super(context, attrs);
        setBackgroundColor(Color.BLUE);
    }

    @Override
    public void onClick(long time){

        if ((time%2) == 0) winnerClicks++;
        else loserClicks ++;

        setBackgroundColor();
        setText(String.valueOf(winnerClicks));
    }

    @Override
    public void setBackgroundColor(){

        if (winnerClicks>loserClicks) setBackgroundColor(Color.GREEN);
        else setBackgroundColor(Color.RED);

    }

}
